var __ADDRESS__ = "http://127.0.0.1:8000/api-auth/";

var url = {
    get_chats: __ADDRESS__ + "get/chats/",
    send_massage: __ADDRESS__ + "send/massage/"
}

class Onlinechat {
    token = "";
    chat_list = [];
    email = "";

    form_set_email() {
        if (localStorage.getItem("onlinechat_email") !== null) {
            this.email = localStorage.getItem("onlinechat_email");
        } else {
            return "<div class='set-email'>" +
                "<div>pls enter your email :</div>" +
                "<input type='email' name='email'>" +
                "<div class='btn'>save</div>" +
                "</div>";
        }
        return true;
    }

    form_set_email_dec() {
        var t = this;

        /* save email to Storage and load chat from email */
        $("#onlinechat .set-email .btn").click(function () {
            var val = $("#onlinechat .set-email input").val();
            if (val !== "") {
                localStorage.setItem("onlinechat_email", val);
                t.email = val;
                $("#onlinechat .set-email").hide();
                t.load_chats();
            }
        })
    }

    html() {
        var html =
            "<div id='onlinechat'>" +
            "<div class='open'><i class='fa fa-support'></i></div>" +
            "<div class='content' style='display: none'>" +
            "<div class='header'><div class='close-onlinechat'>x</div> </div>" +
            "<ul class='chats'>";
        if (this.form_set_email() !== true) {
            html += this.form_set_email();
        }
        html += "</ul>";
        html += "<div class='send_new'>";
        html += "<from method='post'>" +
            "<input type='text' name='content' placeholder='Enter a massage'> " +
            "<div class='btn'>send</div> " +
            "</from>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        return html
    }

    appendTolist(right_left = 0, data) {
        var content = "<span class='content'>" + data.content + "</span>";

        /* check who answer and set a position */
        if (right_left === 0) {
            content = "<li class='right'><span class='name'>you : </span>" + content + "</li>";
        } else {
            content = "<li class='left'><span class='name'>support : </span>" + content + "</li>";
        }
        $("#onlinechat > .content ul.chats").append(content);
    }

    set_style() {
        var style = [
            {
                tag: "#onlinechat",
                css: {
                    position: "fixed",
                    bottom: "20px",
                    right: "20px"
                }
            },
            {
                tag: "#onlinechat .open",
                css: {
                    padding: "10px",
                    background: "#fa8e0a",
                    color: "#FFF",
                    borderRadius: "100px",
                    width: "10px",
                    height: "10px",
                    cursor: "pointer"
                }
            },
            {
                tag: "#onlinechat > .content",
                css: {
                    width: "300px",
                    height: "400px",
                    boxShadow: "0 0 5px #aaa",
                    border: "2px solid #fa8e0a",
                    borderRadius: "5px",
                    background: "#FFF"
                }
            },
            {
                tag: "#onlinechat > .content .header",
                css: {
                    padding: "10px",
                    background: "#fa8e0a"
                }
            },
            {
                tag: "#onlinechat > .content .header .close-onlinechat",
                css: {
                    width: "20px",
                    height: "20px",
                    textAlign: "center",
                    fontSize: "12px",
                    background: "#fff",
                    color: "#000",
                    display: "inline-block",
                    borderRadius: "100px",
                    cursor: "pointer"
                }
            },
            {
                tag: "#onlinechat > .content .send_new input[type='text']",
                css: {
                    width: "200px",
                    border: "1px solid #e37b0b",
                    padding: "5px",
                    borderRadius: "10px",
                    boxShadow: "inset 0 0 5px #aaa"
                }
            },
            {
                tag: "#onlinechat > .content .send_new .btn",
                css: {
                    width: "70px",
                    background: "#ffffff",
                    border: "1px solid #e37b0b",
                    padding: "5px",
                    borderRadius: "10px",
                    textAlign: "center",
                    display: "inline-block",
                    cursor: "pointer"
                }
            },
            {
                tag: "#onlinechat > .content .send_new",
                css: {
                    background: "#fa8e0a",
                    padding: "5px 0"
                }
            },
            {
                tag: "#onlinechat > .content ul.chats",
                css: {
                    height: "297px",
                    padding: "10px",
                    overflow: "auto",
                    margin: "unset"
                }
            },
            {
                tag: "#onlinechat .content ul.chats li .content",
                css: {
                    background: "#ccffee",
                    borderBottom: "1px solid #84c7b3",
                    margin: "5px",
                    padding: "5px",
                    display: "inline-block",
                    borderRadius: "10px"
                }
            },
            {
                tag: "#onlinechat .content ul.chats li .name",
                css: {
                    display: "block",
                    fontSize: '12px',
                    color: "#aaaaaa"
                }
            },
            {
                tag: "#onlinechat .content ul.chats li.right",
                css: {
                    textAlign: "right"
                }
            },
            {
                tag: "#onlinechat .content .set-email",
                css: {
                    background: "#fa8e0a",
                    color: "#FFF",
                    borderRadius: "10px",
                    padding: "10px"
                }
            },
            {
                tag: "#onlinechat .content .set-email .btn",
                css: {
                    background: "#c6063e",
                    color: "#FFF",
                    borderRadius: "10px",
                    padding: "5px",
                    width: "100px",
                    textAlign: "center",
                    cursor: "pointer"
                }
            },
            {
                tag: "#onlinechat > .content ul.chats",
                css: {
                    listStyle: "none",
                    width: "100%"
                }
            }
        ];
        /* render style */
        for (var i = 0; i < style.length; i++) {
            $(style[i].tag).css(style[i].css);
        }
    }

    load_chats() {
        var t = this;
        $.ajax({
            url: url.get_chats,
            data: {token: this.token, email: t.email},
            type: "POST",
            dataType: "json",
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i].fields;
                    /* check append before this or not */
                    var append = true;
                    for (var x = 0; x < t.chat_list.length; x++) {
                        if (data[i].pk === t.chat_list[x].pk) { /* check id in 'chat_list' */
                            append = false;
                            break;
                        }
                    }
                    /* end check */
                    if (append) {
                        t.appendTolist(item.answer, item);
                        t.set_style();
                    }
                }
                t.chat_list = data;
            }
        });
    }

    load_actions() {
        var t = this;

        /* open online chat */
        $("#onlinechat .open").click(function () {
            $("#onlinechat .content").slideDown(1000);
            $(this).hide();
        });

        /* close online chat */
        $("#onlinechat .content .close-onlinechat").click(function () {
            $("#onlinechat .content").slideUp(1000);
            $("#onlinechat .open").delay(1000).slideDown(500);
        });

        /* send new massage */
        $("#onlinechat .content .send_new .btn").click(function () {
            var input = $("#onlinechat .content .send_new input");
            if (input.val() !== "") {
                $.ajax({
                    url: url.send_massage,
                    data: {token: t.token, email: t.email, content: input.val()},
                    type: "post",
                    success: function (res) {
                        /* cleaning the input */
                        input.val("");

                        /* reload chat */
                        t.load_chats();
                    }
                });
            }
        });
    }
}


var main = function (token) {
    var onlinechat = new Onlinechat();
    onlinechat.token = token;

    $("body").append(onlinechat.html());

    onlinechat.set_style();
    onlinechat.form_set_email_dec();
    onlinechat.load_actions();

    /* load chats */
    onlinechat.load_chats();
    setInterval(function () {
        onlinechat.load_chats()
    }, 10000);

}


