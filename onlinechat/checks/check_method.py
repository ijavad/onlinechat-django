from django.shortcuts import HttpResponse


def method_post(func=None):
    def decorator(request, *args, **kwargs):
        if request.method == "POST":
            return func(request, *args, **kwargs)
        return HttpResponse("403 Access denied")

    return decorator
