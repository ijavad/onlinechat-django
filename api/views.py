from django.shortcuts import render, HttpResponse
from .decorators.chat import chat
from onlinechat.checks import check_method
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json


@csrf_exempt
@check_method.method_post
def get_chats(request):
    ch = chat(token=request.POST["token"])
    data = serializers.serialize("json", ch.get(emailMember=request.POST["email"]))
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
@check_method.method_post
def send_massage(request):
    ch = chat(token=request.POST["token"])
    ch.send(emailMember=request.POST["email"],content=request.POST["content"])
    data = {
        "success": 1,
        "text": "success send massage"
    }
    return HttpResponse(json.dumps(data), content_type="application/json")


def test(request):
    return render(request, "test.html")
