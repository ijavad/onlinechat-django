# Generated by Django 3.1.3 on 2020-11-16 13:01

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_chat_email'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.UUIDField(default=uuid.uuid4, help_text='token')),
                ('email', models.CharField(help_text='username', max_length=200)),
                ('name', models.CharField(help_text='first name', max_length=200)),
            ],
        ),
        migrations.RemoveField(
            model_name='chat',
            name='email',
        ),
        migrations.AddField(
            model_name='chat',
            name='member',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.member'),
        ),
    ]
