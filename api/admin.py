from django.contrib import admin
from .models import Tokens, Chat, Member


# Register your models here.
@admin.register(Tokens)
class TokensAdmin(admin.ModelAdmin):
    fields = [("username", "token"), "domain"]
    list_display = ("username", "token", "domain")


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ("answer", "token", "member_email", "date")


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ("email", "name")
