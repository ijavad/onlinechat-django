from django.db import models
from django.contrib.auth.models import User
from uuid import uuid4
from django.utils import timezone


# Create your models here.
class Tokens(models.Model):
    token = models.UUIDField(default=uuid4, help_text="token")
    username = models.ForeignKey(User, on_delete=models.CASCADE, null=True, help_text="username")
    domain = models.CharField(max_length=200, help_text="domain")


class Member(models.Model):
    token = models.ForeignKey(Tokens, on_delete=models.CASCADE, null=True, help_text="token")
    email = models.CharField(max_length=200, help_text="username")
    name = models.CharField(max_length=200, help_text="first name")


class Chat(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    token = models.ForeignKey(Tokens, on_delete=models.CASCADE, null=True, help_text="token")
    content = models.TextField()

    def member_email(self):
        return self.member.email

    NO = 0
    YES = 1

    Answer_CHOICES = (
        (NO, 'No'),
        (YES, 'Yes'),
    )
    answer = models.IntegerField(default=0, choices=Answer_CHOICES)
    date = models.DateTimeField(default=timezone.now)
