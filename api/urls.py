from django.urls import path
from .views import *

urlpatterns = [
    path('get/chats/', get_chats, name="get-chats"),
    path('send/massage/', send_massage, name="send-massage"),
    path('test', test, name="test"),
]
