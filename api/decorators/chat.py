from .base import Base
from api.models import Chat, Member, Tokens


class chat(Base):
    model = Chat

    def get(self, emailMember):
        if self.token != False:
            return self.model.objects.filter(token__token=self.token, member__email=emailMember)
        return {
            "error_code": 403,
            "error_information": "Access denied !"
        }

    def send(self, emailMember, content, answer=0):
        if self.token != False:
            check = Member.objects.filter(email=emailMember)
            if len(check) > 0:
                c = Chat(member=check[0], token=check[0].token, content=content, answer=answer)
                c.save()
        return {
            "error_code": 403,
            "error_information": "Access denied !"
        }
