from api.models import Tokens


class Base:
    """
    check api token its ok or not
    """
    token = False
    userData = []

    def __init__(self, token):
        check = Tokens.objects.filter(token=token)
        if len(check) == 1:
            self.token, self.userData = [token, check[0]]

